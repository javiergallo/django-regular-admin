from django.conf.urls import patterns, include, url
from django_regular_admin.admin import regular_admin_site

urlpatterns = patterns('',
    url(r'^', include(regular_admin_site.urls)),
)
