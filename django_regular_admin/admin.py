from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.forms import AuthenticationForm


class RegularAdminSite(AdminSite):
    """
    Just like an AdminSite object, a RegularAdminSite object encapsulates an
    instance of the Django admin application, ready to be hooked in to your
    URLconf. Models are registered with the RegularAdminSite using the
    register() method, and the get_urls() method can then be used to access
    Django view functions that present a full admin interface for the
    collection of registered models.

    The difference with the AdminSite class is that RegularAdminSite instances
    don't require the users to be staff members to log in. This implementation
    is based on
    http://blog.tryolabs.com/2012/06/18/django-administration-interface-non-staff-users/
    """

    # By default login_form is set to None. Then, the login method uses
    # AdminAuthenticationForm, which checks if the user is staff during login.
    # So, we set AuthenticationForm as login_form which only makes sure that
    # the user is active.
    login_form = AuthenticationForm

    def has_permission(self, request):
        """
        Returns True if the given HttpRequest has permission to view
        *at least one* page in the admin site.

        Requests with inactive users do NOT have permission.
        """

        return request.user.is_active

regular_admin_site = RegularAdminSite(name='regular_admin')
